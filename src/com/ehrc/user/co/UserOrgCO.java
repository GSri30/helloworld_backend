package com.ehrc.user.co;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserOrgCO {
	int id;
	int user_id;
	int org_id;
	String org_name;
	String org_remarks;
	String status;
	
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public String getOrg_remarks() {
		return org_remarks;
	}
	public void setOrg_remarks(String org_remarks) {
		this.org_remarks = org_remarks;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getOrg_id() {
		return org_id;
	}
	public void setOrg_id(int org_id) {
		this.org_id = org_id;
	}
	public String getOrg_name() {
		return org_name;
	}
	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "UserOrgCO [id=" + id + ", user_id=" + user_id + ", org_id=" + org_id + ", org_name=" + org_name
				+ ", org_remarks=" + org_remarks + ", status=" + status + "]";
	}
	
}
