package com.ehrc.user.dao;

import java.text.ParseException;
import java.util.List;

import com.ehrc.db.UserDb;
import com.ehrc.db.UserOrgDb;
import com.ehrc.user.co.UserCO;
import com.ehrc.user.co.UserOrgCO;
import com.ehrc.user.co.UserWithOrgCO;
import com.ehrc.utility.Page;

public interface UserDAO {
	
	public UserCO saveUser(UserCO user) throws ParseException;
	public boolean updateUser(int userId, String role, String status);
	public UserDb getUser(String username);
	public UserOrgCO saveUserOrg(UserOrgCO objUserOrgCO);
	public List<UserOrgDb> getUserOrg(int orgId);
	public UserDb getUserById(int userId);
	public List<UserDb> getAllUserPass();
	public boolean updatePassword(UserCO objUserCO);
	public boolean updatePass(int userId, String password);
	public Page<UserCO> getAllUser(int pageNumber, int pageSize);
	public boolean updateUser(UserDb objUserDb);
	public String getStatus(int userId);
}
