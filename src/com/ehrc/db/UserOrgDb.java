package com.ehrc.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ehrc.user.co.UserOrgCO;

@Entity
@Table(name = "userorgdb")
public class UserOrgDb {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	@Column(name = "user_id")
	int user_id;
	@Column(name = "org_id")
	int org_id;
	@Column(name = "org_remarks")
	String org_remarks;
	@Column(name = "status")
	String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getOrg_id() {
		return org_id;	
	}
	public void setOrg_id(int org_id) {
		this.org_id = org_id;
	}
	
	public String getOrg_remarks() {
		return org_remarks;
	}
	public void setOrg_remarks(String org_remarks) {
		this.org_remarks = org_remarks;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static UserOrgDb mapUserOrgCOToDB(UserOrgCO objUserOrgCO) {
		UserOrgDb objUserOrgDb = new UserOrgDb();
		objUserOrgDb.setId(objUserOrgCO.getId());
		objUserOrgDb.setOrg_id(objUserOrgCO.getOrg_id());
		objUserOrgDb.setUser_id(objUserOrgCO.getUser_id());
		objUserOrgDb.setOrg_remarks(objUserOrgCO.getOrg_remarks());
		objUserOrgDb.setStatus(objUserOrgCO.getStatus());
		return objUserOrgDb;
	}
	
	public static UserOrgCO mapUserOrgDBToCO(UserOrgDb objUserOrgDb){
		
		UserOrgCO objUserOrgCo = new UserOrgCO();
		objUserOrgCo.setId(objUserOrgDb.getId());
		objUserOrgCo.setOrg_id(objUserOrgDb.getOrg_id());
		objUserOrgCo.setUser_id(objUserOrgDb.getUser_id());
		objUserOrgCo.setOrg_remarks(objUserOrgDb.getOrg_remarks());
		objUserOrgCo.setStatus(objUserOrgDb.getStatus());
		return objUserOrgCo;
	}
	
	

}
