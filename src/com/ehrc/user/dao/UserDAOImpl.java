package com.ehrc.user.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ehrc.db.UserDb;
import com.ehrc.db.UserOrgDb;
import com.ehrc.user.co.UserCO;
import com.ehrc.user.co.UserOrgCO;
import com.ehrc.utility.HibernateUtil;
import com.ehrc.utility.Page;
import com.ehrc.utility.PageImpl;

public class UserDAOImpl implements UserDAO{

	Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);
	
	@Override
	public UserCO saveUser(UserCO objUserCO){
		Session ses = null;
		Transaction tx = null;
		UserCO finalResponse = new UserCO();
		// get Session
		ses = HibernateUtil.getSession();
		
		if(objUserCO == null) {
			logger.info(" People Object is null");
			
		} else {
			
			try	{
				UserDb objUserDb = UserDb.mapUserCOToDB(objUserCO);
				objUserDb.setCreated_at(new Date());
				logger.info("Created Date ="+objUserDb.getCreated_at());
				logger.info("User record to be created -> "+objUserDb.getUsername());
				
				tx = ses.beginTransaction();
				ses.saveOrUpdate(objUserDb);
				logger.info("User record created");
				tx.commit();
				
				
				finalResponse.setUsername(objUserDb.getUsername());
				finalResponse.setPassword(objUserDb.getPassword());
				finalResponse.setRole(objUserDb.getRole());
				finalResponse.setMobileNo(objUserDb.getMobile_no());
				finalResponse.setEmail(objUserDb.getEmail());
				finalResponse.setFirstName(objUserDb.getFirst_name());
				finalResponse.setLastName(objUserDb.getLast_name());
				
				
			} catch (Exception e) {
				logger.error("User record creation problem", e);
				tx.rollback();
				finalResponse = null;

			} finally {
				HibernateUtil.closeSession();
			}
		}
		return finalResponse;
	}

	@Override
	public UserDb getUser(String username) {
		Session ses = null;
		UserDb userDomain = null;
		// get Session

		logger.info("Fetching data for username =>>>>>> " + username);
		try {
			ses = HibernateUtil.getSession();
			
			TypedQuery<UserDb> query = ses.createQuery("Select u from UserDb u where LOWER(u.username) = :username", UserDb.class);
			query.setParameter("username", username.toLowerCase());
			userDomain = query.getSingleResult();
		} catch (Exception e) {
			logger.error("ID not found ::", e);
		}finally {
			HibernateUtil.closeSession();
		}
		return userDomain;
	}
	
	@Override
	public UserDb getUserById(int userId) {
		Session ses = null;
		UserDb userDomain = null;
		// get Session

		logger.info("Fetching data for userID =>>>>>> " + userId);
		try {
			ses = HibernateUtil.getSession();
			
			TypedQuery<UserDb> query = ses.createQuery("Select u from UserDb u where u.userId = :userId", UserDb.class);
			query.setParameter("userId", userId);
			userDomain = query.getSingleResult();
		} catch (Exception e) {
			logger.error("ID not found ::", e);
		}finally {
			HibernateUtil.closeSession();
		}
		return userDomain;
	}
	
	@Override
	public UserOrgCO saveUserOrg(UserOrgCO objUserOrgCO){
		Session ses = null;
		Transaction tx = null;
		UserOrgCO finalResponse = new UserOrgCO();
		// get Session
		ses = HibernateUtil.getSession();
		
		if(objUserOrgCO == null) {
			logger.info(" People Object is null");
			
		} else {
			
			try	{
				UserOrgDb objUserOrgDb = UserOrgDb.mapUserOrgCOToDB(objUserOrgCO);
				logger.info("User Org record to be created");
				
				tx = ses.beginTransaction();
				ses.saveOrUpdate(objUserOrgDb);
				logger.info("User Org record created>>");
				tx.commit();
				
				
				finalResponse.setUser_id(objUserOrgDb.getUser_id());
				finalResponse.setOrg_id(objUserOrgDb.getOrg_id());
				finalResponse.setOrg_remarks(objUserOrgDb.getOrg_remarks());
				
			} catch (Exception e) {
				logger.error("User Org 	record creation problem", e);
				tx.rollback();
				finalResponse = null;

			} finally {
				HibernateUtil.closeSession();
			}
		}
		return finalResponse;
	}

	@Override
	public Page<UserCO> getAllUser(int pageNumber, int pageSize) {
		Session ses = null;
		Page<UserCO> returnValue = null;
		
		List<UserDb> userDomain;
		UserOrgCO[] arrUserOrgCo = null;
		List<UserCO> userCOList = new ArrayList<UserCO>();
		// get Session

		logger.info("Fetching All user data ");
		try {
			ses = HibernateUtil.getSession();
			
			String countQ = "Select count (*) from UserDb u";
			Query countQuery = ses.createQuery(countQ);
			long countResults = (long) countQuery.getSingleResult();
			int lastPageNumber = (int) (Math.ceil(countResults / pageSize));
			
			TypedQuery<UserDb> query = ses.createQuery("Select u from UserDb u", UserDb.class);
			query.setFirstResult(pageSize*pageNumber);
			query.setMaxResults(pageSize);			
			logger.info("Fetching all user data" + query);
			userDomain = query.getResultList();
			if(userDomain != null && userDomain.size() > 0) {
				logger.info("Total User Count => " + userDomain.size());
				List<UserOrgDb> objuserOrgDomain;
				for(UserDb objUser : userDomain) {
					logger.info("Fetching org for user => " + objUser.getUserId());
					TypedQuery<UserOrgDb> userOrgQuery = ses.createQuery("Select uod from UserOrgDb uod where uod.user_id = :userid", UserOrgDb.class);
					userOrgQuery.setParameter("userid", objUser.getUserId());
					objuserOrgDomain = userOrgQuery.getResultList();
					if(objuserOrgDomain != null && objuserOrgDomain.size() > 0) {
						
						logger.info("Total org for user "+ objUser.getUserId() +" = " + objuserOrgDomain.size());
						arrUserOrgCo = new UserOrgCO[objuserOrgDomain.size()];
						int i =0;
						
					}
					
				
					
				}
			}
			
			
			
			
			
			returnValue = new PageImpl<UserCO>(userCOList, countResults, pageSize, userCOList.size(), lastPageNumber);
			
			
		} catch (Exception e) {
			logger.error("ID not found ::", e);
		}finally {
			HibernateUtil.closeSession();
		}
		return returnValue;
	}

	
	@Override
	public List<UserDb> getAllUserPass() {
		Session ses = null;
		List<UserDb> userDomain = null;
		// get Session

		logger.info("Fetching password data for all user =>>>>>> ");
		try {
			ses = HibernateUtil.getSession();
			
			TypedQuery<UserDb> query = ses.createQuery("Select u from UserDb u", UserDb.class);
			logger.info("Fetching user data for orgId query =>>>>>> " + query);
			userDomain = query.getResultList();
		} catch (Exception e) {
			logger.error("ID not found ::", e);
		}finally {
			HibernateUtil.closeSession();
		}
		return userDomain;
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean updatePassword(UserCO objUserCO) {
		Session ses = null;
		Transaction tx = null;
		int result = 0;
		boolean response = false;
		// get Session

		logger.info("**Updating password for all users with Salt**");
		try {
			
			ses = HibernateUtil.getSession();
			tx = ses.beginTransaction();
				UserDb objUserDb = new UserDb();
				objUserDb = objUserDb.mapUserCOToDB(objUserCO);
				
				Query query = ses.createQuery("update UserDb u set u.password = pgp_sym_encrypt(:password, 'abcd'), u.updated_at = :updated_at where u.userId = :userId");
				query.setParameter("password", objUserDb.getPassword());
				query.setParameter("updated_at", objUserDb.getUpdated_at());
				query.setParameter("userId", objUserDb.getUserId());
				
				System.out.println("UserId = >>"+objUserDb.getUserId());
				
				
				result = query.executeUpdate();
				
				if(result != 0 ) {
					tx.commit();
					query = null;
					response = true;
					
				} else {
					tx.rollback();
					response = false;
				}
		} catch (Exception e) {
			logger.error("ID not found ::", e);
			tx.rollback();
			response = false;
		}finally {
			HibernateUtil.closeSession();
		}
		
		return response;
	}

	@Override
	public List<UserOrgDb> getUserOrg(int orgId) {
		Session ses = null;
		List<UserOrgDb> userOrgDomain = null;
		// get Session

		logger.info("Fetching user data for orgId =>>>>>> " + orgId);
		try {
			ses = HibernateUtil.getSession();
			
			TypedQuery<UserOrgDb> query = ses.createQuery("Select u from UserOrgDb u where u.org_id = :orgId", UserOrgDb.class);
			query.setParameter("orgId", orgId);
			
			logger.info("Fetching user data for orgId query =>>>>>> " + query);
			userOrgDomain = query.getResultList();
		} catch (Exception e) {
			logger.error("ID not found ::", e);
		}finally {
			HibernateUtil.closeSession();
		}
		return userOrgDomain;
	}

	
	@Override
	public String getStatus(int userId) {
		Session ses = null;
		UserDb objUser = null;
		String status = null;
		
		logger.info("Fetching user data for userId =>>>>>> " + userId);
		try {
			ses = HibernateUtil.getSession();
			TypedQuery<UserDb> query = ses.createQuery("Select u from UserDb u where u.userId = :userId", UserDb.class);
			query.setParameter("userId", userId);			
			logger.info("Fetching user data for orgId query =>>>>>> " + query);
			objUser = query.getSingleResult();
			
			if(objUser != null) {
				status = objUser.getStatus();
			}
			
		} catch(Exception e) {
			logger.error("ID not found :: ",e);
		} finally {
			HibernateUtil.closeSession();
		}
		return status;
	}

	@Override
	public boolean updateUser(UserDb objUserDb) {
		Session ses = null;
		Transaction tx = null;
		boolean response = false;
		// get Session

		logger.info("**Updating user for all users**");
		try {
			ses = HibernateUtil.getSession();
			tx = ses.beginTransaction();
			int result = 0;
			
			if(objUserDb != null) {
				
				Query query = ses.createQuery("update UserDb u set u.email = pgp_sym_encrypt(:email, 'abcd'), u.status = :status, u.designation = :designation, u.mobile_no = pgp_sym_encrypt(:mobile_no, 'abcd'), u.updated_at = :updated_at where u.userId = :userId");
				query.setParameter("email", objUserDb.getEmail());
				query.setParameter("mobile_no", objUserDb.getMobile_no());
				query.setParameter("status", objUserDb.getStatus());
				query.setParameter("designation", objUserDb.getDesignation());
				query.setParameter("updated_at", new Date());
				query.setParameter("userId", objUserDb.getUserId());
				
				System.out.println("UserId for password update = >>"+objUserDb.getUserId());
				
				result = query.executeUpdate();
				
				if(result != 0 ) {
					
					tx.commit();
					logger.info("User updated.");
					query = null;
					response = true;
					
				} else {
					tx.rollback();
					response = false;
				}
			}
		} catch (Exception e) {
			logger.error("ID not found ::", e);
			tx.rollback();
			response = false;
		}finally {
			HibernateUtil.closeSession();
		}
		return response;
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean updatePass(int userId, String password) {
		Session ses = null;
		Transaction tx = null;
		int result = 0;
		boolean response = false;
		// get Session

		logger.info("**Updating password for user**");
		logger.info("User ID = "+userId);
		try {
			
			ses = HibernateUtil.getSession();
			tx = ses.beginTransaction();
								
				Query query = ses.createQuery("update UserDb u set u.password = pgp_sym_encrypt(:password, 'abcd'), u.updated_at = :updated_at where u.userId = :userId");
				query.setParameter("password", password);
				query.setParameter("updated_at", new Date());
				query.setParameter("userId", userId);
				
				System.out.println("UserId for password update = >>"+userId);
				
				result = query.executeUpdate();
				
				if(result != 0 ) {
					tx.commit();
					query = null;
					response = true;
					
				} else {
					tx.rollback();
					response = false;
				}
		} catch (Exception e) {
			logger.error("ID not found ::", e);
			tx.rollback();
			response = false;
		}finally {
			HibernateUtil.closeSession();
		}
		
		return response;
	}

	@Override
	public boolean updateUser(int userId, String role, String status) {
		Session ses = null;
		Transaction tx = null;
		int result = 0;
		boolean response = false;
		// get Session

		logger.info("**Updating user for all users**");
		try {
			
			ses = HibernateUtil.getSession();
			tx = ses.beginTransaction();
			
				Query query = ses.createQuery("update UserDb u set u.role = :role, u.status = :status, u.updated_at=:updated_at where u.userId = :userId");
				query.setParameter("role", role);
				query.setParameter("status", status);
				query.setParameter("userId", userId);
				query.setParameter("updated_at", new Date());
				
				System.out.println("UserId = >>"+userId);
				
				result = query.executeUpdate();
				
				if(result != 0 ) {
					tx.commit();
					query = null;
					response = true;
					
				} else {
					tx.rollback();
					response = false;
				}
		} catch (Exception e) {
			logger.error("ID not found ::", e);
			tx.rollback();
			response = false;
		}finally {
			HibernateUtil.closeSession();
		}
		System.out.println("Result = "+response);
		return response;
	}
}
